
#include <dssi.h>
#include "common.h"

// TODO: Possibly make this injectable by changing what source files are
// included that implement the required methods.
// We'd have code here that reads those methods to construct the descriptors
// and handle the calls from the host so that our implementation gets its
// state updated automagically.

static const char *portNames[2] = {
	"Left Output",
	"Right Output",
	"Midi Input",
};

static LADSPA_PortDescriptor portDescriptors[2] = {
	LADSPA_PORT_OUTPUT | LADSPA_PORT_AUDIO,
	LADSPA_PORT_OUTPUT | LADSPA_PORT_AUDIO,
	LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL,
};

static LADSPA_PortRangeHint portRangeHints[2] = {
	{
		.HintDescriptor = LADSPA_HINT_DEFAULT_NONE,
		.LowerBound = -1.0f,
		.UpperBound = 1.0f,
	},
	{
		.HintDescriptor = LADSPA_HINT_DEFAULT_NONE,
		.LowerBound = -1.0f,
		.UpperBound = 1.0f,
	},
	{
		.HintDescriptor = LADSPA_HINT_DEFAULT_NONE,
		.LowerBound = -1.0f,
		.UpperBound = 1.0f,
	},
};

static LADSPA_Descriptor ladspaDescriptor = {
	.UniqueID = 0x00123456,
	.Label = "test_instrument",
	.Properties = 0,
	.Name = "Test Instrument",
	.Maker = "David Laurie",
	.Copyright = "2020 David Laurie",
	.PortCount = 2,
	.PortDescriptors = portDescriptors,
	.PortNames = portNames,
	.PortRangeHints = portRangeHints,
	.ImplementationData = nullptr,
	
	.instantiate = nullptr,
	.connect_port = nullptr,
	.activate = nullptr,
	.run = nullptr,
	.run_adding = nullptr,
	.set_run_adding_gain = nullptr,
	.deactivate = nullptr,
	.cleanup = nullptr,
};

static DSSI_Descriptor dssiDescriptor = {
	.DSSI_API_Version = 1,
	.LADSPA_Plugin = &ladspaDescriptor,
	
	.configure = nullptr,
	.get_program = nullptr,
	.select_program = nullptr,
	.get_midi_controller_for_port = nullptr,
	.run_synth = nullptr,
	.run_synth_adding = nullptr,
	.run_multiple_synths = nullptr,
	.run_multiple_synths_adding = nullptr,
};

extern "C" DLL_EXPORT const DSSI_Descriptor *dssi_descriptor(unsigned long Index)
{
	switch (Index)
	{
		case 0:
			return &dssiDescriptor;
		default:
			return nullptr;
	}
}

extern "C" DLL_EXPORT const LADSPA_Descriptor *ladspa_descriptor(unsigned long Index)
{
	switch (Index)
	{
		case 0:
			return &ladspaDescriptor;
		default:
			return nullptr;
	}
}

