
// Base frequencies for octave
static const float baseNoteFrequencies[12] = {
	261.625580, // C 4
	277.182617, // C#4
	293.664764, // D 4
	311.126984, // D#4
	329.627563, // E 4
	349.228241, // F 4
	369.994415, // F#4
	391.995422, // G 4
	415.304688, // G#4
	440.000000, // A 4
	466.153757, // A#4
	493.883301, // B 4
}

static const float _octaveMultipliers[11] = {
	0.03125f, // -1
	0.0625f, // 0
	0.125f, // 1
	0.25f, // 2
	0.5f, // 3
	1.0f, // 4
	2.0f, // 5
	4.0f, // 6
	8.0f, // 7
	16.0f, // 8
	32.0f, // 9
}

// Indexable by the actual octave, ranging from -1 to 9 inclusive.
static const float * const octaveMultipliers = &_octaveMultipliers[1];

class NoteData
{
	// TODO: A frequency table for all midi notes.
	// Maybe also some samplerate dependent tables, like period of a note, or
	// something 2pi related for lookups, or whatever.
}

