CPPFLAGS=-Iinc -c -Wall -Werror -fpic
CPP=g++
OBJ=obj
SRC=src
LIB=lib

.PHONY: clean

clean:
	mkdir -p $(OBJ)
	mkdir -p $(LIB)
	rm -f $(OBJ)/*.o
	rm -f $(LIB)/*.so

$(OBJ)/%.o: $(SRC)/%.cpp
	$(CPP) $(CPPFLAGS) -o $@ $< $(CPPFLAGS)

TEST_INSTRUMENT_OBJ=$(OBJ)/test_instrument.o
test_instrument: $(TEST_INSTRUMENT_OBJ)
	$(CPP) -shared -o $(LIB)/test_instrument.so $(TEST_INSTRUMENT_OBJ)

all: test_instrument

